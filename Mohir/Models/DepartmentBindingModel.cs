﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models
{
    public class DepartmentBindingModel
    {
        [Required]
        public string DepartmentName { get; set; }
        [Required]
        public int ManagerId { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }

    public class UpdateDepBindingModel:DepartmentBindingModel
    {
        public int DepartmentId { get; set; }
    }
}