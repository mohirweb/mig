﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Mohir.Models.HRTables;

namespace Mohir.Models
{
    public class HRContext : DbContext
    {
        public HRContext()
            : base("HRContext")
        {
          
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
        }

        public virtual DbSet<AttendanceList> AttendanceLists { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<EmployeeJob> EmployeeJobs { get; set; }
    }
}
