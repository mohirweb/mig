﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mohir.Models.HRTables
{
    public class Gender
    {
       
        public Gender()
        {
            this.Employees = new HashSet<Employee>();
        }

        public int GenderId { get; set; }
        public string Name { get; set; }

       
        public virtual ICollection<Employee> Employees { get; set; }
    }
}