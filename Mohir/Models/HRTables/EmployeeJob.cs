﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models.HRTables
{
    public  class EmployeeJob
    {
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int JobId { get; set; }
        public System.DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Job Job { get; set; }
    }
}