﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace Mohir.Models.HRTables
{
    public  class Employee
    {
        
       
        [Key]
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int MaritalStatus { get; set; }
        public int GenderId { get; set; }
        public System.DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string PassportSerial { get; set; }
        public System.DateTime HireDate { get; set; }
        public decimal Salary { get; set; }
        public string Photo { get; set; }
        public string Cv { get; set; }
        public string Contract { get; set; }
        public string Passport { get; set; }
        public string Diploma { get; set; }

       
        
        public virtual Gender Gender { get; set; }
        //public virtual AttendanceList AttendanceList { get; set; }
       public ICollection<AttendanceList> AttendanceLists { get; set; }

       public Employee()
       {
           AttendanceLists = new List<AttendanceList>();
           EmployeeJobs = new HashSet<EmployeeJob>();
        }

        public virtual ICollection<EmployeeJob> EmployeeJobs { get; set; }
    }
}