﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models.HRTables
{
    public class Job
    {
        public Job()
        {
            this.EmployeeJobs = new HashSet<EmployeeJob>();
        }
        [Key]
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
        public int DepartmentId { get; set; }
        public int? CreatedId { get; set; }

        public virtual Department Department { get; set; }

        public virtual ICollection<EmployeeJob> EmployeeJobs { get; set; }


    }
}