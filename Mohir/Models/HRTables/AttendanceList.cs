﻿using System.Collections.Generic;
using Mohir.Models.HRTables;

namespace Mohir.Models
{
    public class AttendanceList
    {
        public int Id { get; set; }

        public System.DateTime Arrivaltime { get; set; }
        public System.DateTime? Departuretime { get; set; }
        public int? EmployeeId { get; set; }
        public Employee Employee { get; set; }

       

    }
}