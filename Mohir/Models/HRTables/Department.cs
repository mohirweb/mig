﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models.HRTables
{
    public class Department
    {
        
        [Key]
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? ManagerId { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedId { get; set; }

        public virtual ICollection<Job> Jobs { get; set; }
    }
}