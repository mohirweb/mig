﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models
{
    public class RoleBindingModel
    {
        [Required(ErrorMessage = "RoleName is Required")]
        public string RoleName { get; set; }
    }
}