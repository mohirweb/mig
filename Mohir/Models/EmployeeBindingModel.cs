﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Mohir.Validators;

namespace Mohir.Models
{

    public class EmployeeBindingModel
    {

        [Required(ErrorMessage = "FirstName is Required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "MiddleName is Required")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "LastName is Required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "genderId is Required")]
        public int GenderId { get; set; }

        [Required(ErrorMessage = "Please select Marital status")]


        public System.DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public System.DateTime HireDate { get; set; }
        public decimal Salary { get; set; }
        public string PassportSerial { get; set; }
        public string Address { get; set; }
        public int JobId { get; set; }
    }

    public class EmployeeUpdateBindingModel : Documents
    {
        public int EmployeeId { get; set; }
        [ValidEnumValue]
        public MaritalStatusEnum MaritalStatus { get; set; }

    }

    public class GetEmployeeByIdBindingModel : Documents
    {
        public int EmployeeId { get; set; }
        public int MaritalStatus { get; set; }
    }



    public class EmployeePostBindingModel : Documents
    {
        [ValidEnumValue]
        public MaritalStatusEnum MaritalStatus { get; set; }


    }

    public class Documents : EmployeeBindingModel
    {
        public string Photo { get; set; }
        public string Cv { get; set; }
        public string Contract { get; set; }
        public string Passport { get; set; }
        public string Diploma { get; set; }
    }

    public enum MaritalStatusEnum
    {
        Single = 1,
        Married = 2
    }
}