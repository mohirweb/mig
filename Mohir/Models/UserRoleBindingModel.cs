﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models
{
    public class UserRoleBindingModel
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public int RoleId { get; set; }

    }
}