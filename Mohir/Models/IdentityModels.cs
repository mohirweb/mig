﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Migrations.Infrastructure;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace Mohir.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser<int, MyUserLogin, MyUserRole, MyUserClaim>,IUser<int>
    {
        public int EmployeeId { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public static implicit operator IdentityUser(ApplicationUser v)
        {
            throw new NotImplementedException();
        }
    }

    public class MyUserRole : IdentityUserRole<int> { }
    public class MyUserClaim : IdentityUserClaim<int> { }
    public class MyUserLogin : IdentityUserLogin<int> { }

    public class MyRole : IdentityRole<int, MyUserRole>, IRole<int>
    {
        public MyRole()
        {

        }

        public MyRole(string name) : this()
        {
            Name = name;
        }
    }

    public class MyUserStore : UserStore<ApplicationUser, MyRole, int,
            MyUserLogin, MyUserRole, MyUserClaim>,IUserStore<ApplicationUser,int>,IDisposable
        {
            public MyUserStore() : this(new ApplicationDbContext())
            {
                base.DisposeContext = true;
            }
            public MyUserStore(DbContext context)
                : base(context)
            {
            }
        }
    

    public class MyRoleStore : RoleStore<MyRole, int, MyUserRole>,IQueryableRoleStore<MyRole,int>,IDisposable
    {
        public MyRoleStore()
            : base(new ApplicationDbContext())
        {
            base.DisposeContext = true;
        }

        public MyRoleStore(DbContext context)
            : base(context)
        {

        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, MyRole, int, MyUserLogin, MyUserRole, MyUserClaim>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            
        }

        public static ApplicationDbContext Create()

        {
            return new ApplicationDbContext();
        }
    }
}