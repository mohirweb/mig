﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mohir.Models
{
    public class JobBindingModel
    {
        [Required(ErrorMessage = "Please Fill the Title")]
        public string JobTitle { get; set; }
        [Required]
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "Please select the Department")]
        public int DepartmentId { get; set; }
    }
    public class JobUpdateBindingModel
    {
        public int JobId { get; set; }
        [Required(ErrorMessage = "Please Fill the Title")]
        public string JobTitle { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Please select the Department")]
        public int DepartmentId { get; set; }
    }
}