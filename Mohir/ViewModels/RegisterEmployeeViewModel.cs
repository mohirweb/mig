﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Mohir.ViewModels
{
    //public class FileNamesViewModel:EmployeeBaseViewModel
    //{
        
        
    //}

    public class EmployeeBaseViewModel
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        public System.DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public System.DateTime HireDate { get; set; }
        public int JobId { get; set; }
        public decimal Salary { get; set; }
        public int ManagerId { get; set; }
        public string Address { get; set; }
        public string PassportSerial { get; set; }
        public string Photo { get; set; }
        public string Cv { get; set; }
        public string Contract { get; set; }
        public string Passport { get; set; }
        public string Diploma { get; set; }

        #region files
        //public HttpPostedFileBase emp_Photo { get; set; }
        //public HttpPostedFileBase emp_Cv { get; set; }
        //public HttpPostedFileBase emp_Contract { get; set; }
        //public HttpPostedFileBase emp_Passport { get; set; }
        //public HttpPostedFileBase emp_Diploma { get; set; }
        #endregion

    }

    public class UploadFilesViewModel: EmployeeBaseViewModel
    {
        public HttpPostedFileBase emp_Photo { get; set; }
        public HttpPostedFileBase emp_Cv { get; set; }
        public HttpPostedFileBase emp_Contract { get; set; }
        public HttpPostedFileBase emp_Passport { get; set; }
        public HttpPostedFileBase emp_Diploma { get; set; }
    }
}