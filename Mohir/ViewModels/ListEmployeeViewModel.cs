﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mohir.ViewModels
{
    public class ListEmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public string Photo { get; set; }
        public string FullName { get; set; }
        public System.DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string PassportSerial { get; set; }
        public decimal Salary { get; set; }
        public string PhotoPath { get; set; }
    }
}