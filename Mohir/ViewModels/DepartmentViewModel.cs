﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mohir.Models;

namespace Mohir.ViewModels
{
    public class DepartmentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

    }
}