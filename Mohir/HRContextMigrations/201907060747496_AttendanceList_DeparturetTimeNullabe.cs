namespace Mohir.HRContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AttendanceList_DeparturetTimeNullabe : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "AttendanceList_Id", "dbo.AttendanceLists");
            DropIndex("dbo.Employees", new[] { "AttendanceList_Id" });
            AlterColumn("dbo.AttendanceLists", "Departuretime", c => c.DateTime());
            AlterColumn("dbo.AttendanceLists", "EmployeeId", c => c.Int());
            CreateIndex("dbo.AttendanceLists", "EmployeeId");
            AddForeignKey("dbo.AttendanceLists", "EmployeeId", "dbo.Employees", "EmployeeId");
            DropColumn("dbo.Employees", "AttendanceList_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "AttendanceList_Id", c => c.Int());
            DropForeignKey("dbo.AttendanceLists", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.AttendanceLists", new[] { "EmployeeId" });
            AlterColumn("dbo.AttendanceLists", "EmployeeId", c => c.Int(nullable: false));
            AlterColumn("dbo.AttendanceLists", "Departuretime", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Employees", "AttendanceList_Id");
            AddForeignKey("dbo.Employees", "AttendanceList_Id", "dbo.AttendanceLists", "Id");
        }
    }
}
