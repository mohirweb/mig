namespace Mohir.HRContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeAttendance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AttendanceLists", "EmployeeId", c => c.Int(nullable: false));
            AddColumn("dbo.Employees", "AttendanceList_Id", c => c.Int());
            CreateIndex("dbo.Employees", "AttendanceList_Id");
            AddForeignKey("dbo.Employees", "AttendanceList_Id", "dbo.AttendanceLists", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "AttendanceList_Id", "dbo.AttendanceLists");
            DropIndex("dbo.Employees", new[] { "AttendanceList_Id" });
            DropColumn("dbo.Employees", "AttendanceList_Id");
            DropColumn("dbo.AttendanceLists", "EmployeeId");
        }
    }
}
