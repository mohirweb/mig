namespace Mohir.HRContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttendanceLists", "Employee_EmployeeId", "dbo.Employees");
            DropIndex("dbo.AttendanceLists", new[] { "Employee_EmployeeId" });
            DropColumn("dbo.AttendanceLists", "Employee_EmployeeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AttendanceLists", "Employee_EmployeeId", c => c.Int());
            CreateIndex("dbo.AttendanceLists", "Employee_EmployeeId");
            AddForeignKey("dbo.AttendanceLists", "Employee_EmployeeId", "dbo.Employees", "EmployeeId");
        }
    }
}
