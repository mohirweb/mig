﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using Mohir.App_Start;
using Mohir.Models;

[assembly: OwinStartup(typeof(Mohir.Startup))]

namespace Mohir
{

    public partial class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            ConfigureAuth(app);
            //CreateUserAndRoles();
            
        }

        //public void CreateUserAndRoles()
        //{
        //    ApplicationDbContext context = new ApplicationDbContext();
        //    //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
        //    //var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

        //    var roleManager = new ApplicationRoleManager(new MyRoleStore(context));
        //    var userManager = new ApplicationUserManager(new MyUserStore());

        //    ////var roleManager = new RoleManager<ApplicationRoleManager>(new RoleStore<MyRoleStore>(context));


        //    if (!roleManager.RoleExists("SuperAdmin"))
        //    {
        //        //Create Default Role
        //        var role = new MyRole("SuperAdmin");
        //        roleManager.Create(role);

        //        //Create Default Users
        //        var user = new ApplicationUser
        //        {
        //            UserName = "SuNu",
        //            Email = "sunu@domain.com",
        //            FirstName = "Nurullo",
        //            LastName = "Sulaymonov"
        //        };
        //        string pwd = "Pass@1234585";

        //       var newuser = userManager.Create(user, pwd);
        //        if (newuser.Succeeded)
        //        {
        //            userManager.AddToRole(user.Id, "admin");

        //        }
        //    }
        //}


        //private void CreateUserAndRoles()
        //{
        //    //app environment configuration
        //    using (var db = new ApplicationDbContext())
        //    {
        //        //db.Database.CreateIfNotExists();
        //        var roleStore = new RoleStore<IdentityRole>(db);
        //        var role = roleStore.FindByNameAsync("Admin").Result;
        //        if (role == null)
        //        {
        //            roleStore.CreateAsync(new IdentityRole("Admin")).Wait();
        //        }

        //        var userStore = new UserStore<IdentityUser>(db);
        //        var manager = new ApplicationUserManager(userStore);

        //        var admin = manager.FindByName("admin");
        //        if (admin == null)
        //        {
        //            admin = new ApplicationUser
        //            {
        //                UserName = "admin",
        //                Email = "admin@sandsea.info",
        //                EmailConfirmed = true,
        //                CreateDate = DateTime.Now
        //            };
        //            var r = manager.CreateAsync(admin, "~Pwd123456").Result;
        //        }
        //        if (!manager.IsInRole(admin.Id, role.Name))
        //        {
        //            manager.AddToRole(admin.Id, role.Name);
        //        }
        //    }
        //}
    }
}