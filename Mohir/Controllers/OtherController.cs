﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mohir.Models;

namespace Mohir.Controllers
{
    public class OtherController : ApiController
    {
        HRContext _context =new HRContext();
        public IHttpActionResult GetGender()
        {
            var gender = _context.Genders.Select(g=> new
            {
                id = g.GenderId,
                gender = g.Name
            }).ToList();
            return Ok(gender);
        }
         public IHttpActionResult GetMaritalStatus()
         {
             var statusList = Enum.GetValues(typeof(MaritalStatusEnum)).Cast<MaritalStatusEnum>().Select(m => new
             {
                 id = ((int) m),
                 name = m.ToString()
             }).ToList();
            return Ok(statusList);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
