﻿using System.Linq;
using System.Web.Http;
using Mohir.Models;
using Mohir.ViewModels;
using System.Data.Entity;
using System.Web;
using Microsoft.AspNet.Identity;
using Mohir.Models.HRTables;

namespace Mohir.Controllers
{
    [Authorize]
    public class DepartmentController : ApiController
    {
        private HRContext _context = new HRContext();
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetDepartment()
        {
            var departments = _context.Departments.Select(d => new DepartmentViewModel()
            {
                Id = d.DepartmentId,
                Name = d.DepartmentName,
                IsActive = d.IsActive
            }).ToList();

            return Ok(departments);

        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetDepartment(int id)
        {
            var department = _context.Departments.Find(id);
            if (department != null)
            {
                return Ok(department);
            }

            return Ok(NotFound());
        }

        [HttpPost]
        public IHttpActionResult PostNewDepartment(DepartmentBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var checkForEmployee = _context.Employees.Find(model.ManagerId);
            if (checkForEmployee != null || model.ManagerId == 0)
            {
                var department = new Department()
                {
                    DepartmentName = model.DepartmentName,
                    IsActive = model.IsActive,
                    ManagerId = model.ManagerId,
                    CreatedId = HttpContext.Current.User.Identity.GetUserId<int>()
                };
                _context.Departments.Add(department);
                _context.SaveChanges();
                return Ok();
            }

            return Ok(BadRequest());

        }

        [HttpPut]
        public IHttpActionResult UpdateDepartment(UpdateDepBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var existingEmp = _context.Departments.Find(model.DepartmentId);
            if (existingEmp != null)
            {
                Department department = new Department()
                {
                    DepartmentName = model.DepartmentName,
                    IsActive = model.IsActive,
                    ManagerId = model.ManagerId
                };
                _context.Entry(department).State = EntityState.Modified;
                _context.SaveChanges();
            }

            return Ok();

        }
    }
}

