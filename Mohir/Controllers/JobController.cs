﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Mohir.Models;
using Mohir.Models.HRTables;
using Mohir.ViewModels;
using EntityState = System.Data.Entity.EntityState;

namespace Mohir.Controllers
{
    public class JobController : ApiController
    {
        public  HRContext _context = new HRContext();
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetJob()
        {

            IList<JobViewModel> jobs = null;
            jobs = _context.Jobs.Select(s => new JobViewModel()
            {
                JobId = s.JobId,
                JobTitle = s.JobTitle
            }).ToList<JobViewModel>();

            return Ok(jobs);
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetJob(int id)
        {

            IList<JobViewModel> jobs = null;
            jobs = _context.Jobs.Where(x => x.JobId == id).Select(s => new JobViewModel()
            {
                JobId = s.JobId,
                JobTitle = s.JobTitle
            }).Where(x => x.JobId == id).ToList<JobViewModel>();

            return Ok(jobs);
        }

        [HttpPost]
        public IHttpActionResult PostNewJob(JobBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var job = new Job()
            {
                JobTitle = model.JobTitle,
                IsActive = model.IsActive,
                DepartmentId = model.DepartmentId,
                CreatedId = HttpContext.Current.User.Identity.GetUserId<int>()
            };
            _context.Jobs.Add(job);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult UpdateJob(JobUpdateBindingModel job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var existingJob = _context.Jobs.Count(s => s.JobId == job.JobId);
            if (existingJob > 0)
            {
                var entry = new Job()
                {
                    JobId = job.JobId,
                    JobTitle =job.JobTitle,
                    DepartmentId = job.DepartmentId,
                    IsActive = job.IsActive,
                    CreatedId = HttpContext.Current.User.Identity.GetUserId<int>()
                };

                _context.Entry(entry).State = EntityState.Modified;
                _context.SaveChanges();
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
