﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Mohir.Models;
using Mohir.Models.HRTables;
using Mohir.Services;
using Mohir.ViewModels;
using Newtonsoft.Json;


namespace Mohir.Controllers
{
    [Authorize]
    public class EmployeeController : ApiController
    {
        private HRContext _context = new HRContext();

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetEmployee()
        {
            IList<ListEmployeeViewModel> employee = null;
            employee = _context.Employees.Select(s => new ListEmployeeViewModel()
            {
                EmployeeId = s.EmployeeId,
                Photo = s.Photo,
                FullName = s.LastName + " " + s.FirstName + " " + s.MiddleName,
                Email = s.Email,
                BirthDate = s.BirthDate,
                MaritalStatus = s.MaritalStatus == 1 ? "Single" : "Married",
                Gender = s.Gender.Name,
                Phone = s.PhoneNumber,
                Adress = s.Address,
                PhotoPath = s.Photo != null ? "/Content/EmployeeDocuments/photo/photo200x200/" + s.Photo : string.Empty,
                PassportSerial = s.PassportSerial,
                Salary = s.Salary
            }).ToList<ListEmployeeViewModel>();

            return Ok(employee);
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetEmployee(int id)
        {
            IList<GetEmployeeByIdBindingModel> employee = null;
            employee = _context.Employees.Include(e => e.EmployeeJobs).Where(x => x.EmployeeId == id).Select(s => new GetEmployeeByIdBindingModel()
            {
                EmployeeId = s.EmployeeId,
                FirstName = s.FirstName,
                LastName = s.LastName,
                MiddleName = s.MiddleName,
                BirthDate = s.BirthDate,
                GenderId = s.Gender.GenderId,
                Email = s.Email,
                Salary = s.Salary,
                PassportSerial = s.PassportSerial,
                Address = s.Address,
                MaritalStatus = s.MaritalStatus,
                HireDate = s.HireDate,
                PhoneNumber = s.PhoneNumber,
                Photo = s.Photo != null ? "/Content/EmployeeDocuments/photo/" + s.Photo : string.Empty,
                Contract = s.Photo != null ? "/Content/EmployeeDocuments/contract/" + s.Contract : string.Empty,
                Diploma = s.Photo != null ? "/Content/EmployeeDocuments/diploma/" + s.Diploma : string.Empty,
                Cv = s.Photo != null ? "/Content/EmployeeDocuments/cv/" + s.Cv : string.Empty,
                Passport = s.Photo != null ? "/Content/EmployeeDocuments/passport/" + s.Passport : string.Empty,

                JobId = s.EmployeeJobs.Select(p => p.JobId).FirstOrDefault()
            }).ToList<GetEmployeeByIdBindingModel>();

            return Ok(employee);


        }

        [HttpPost]
        public async Task<IHttpActionResult> PostEmployee()
        {
            var size = true;
            var type = true;

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

            var json = await filesReadToProvider.Contents[0].ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<EmployeePostBindingModel>(json);

            ModelState.Clear();
            this.Validate(model);
            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }
            var httpRequest = HttpContext.Current.Request;
            var photo = httpRequest.Files["photo"];
            #region Check photo
            if (photo != null)
            {
                size = FileServices.CheckSize(photo);
                if (!size)
                {
                    return Ok("Image Size exceeds maximum size. Foto must be smaller than 2MB!");
                }

                type = FileServices.CheckType(photo, "jpg png jpeg");
                if (!type)
                {
                    return Ok("Image Type must be jpg png or jpeg!");
                }
            }
            #endregion

            var cv = httpRequest.Files["cv"];
            #region Check CV
            if (cv != null)
            {
                size = FileServices.CheckSize(cv);
                if (!size)
                {
                    return Ok("Cv Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(cv, "pdf doc docx");
                if (!type)
                {
                    return Ok("CV Type must be pdf doc or docx!");
                }
            }

            #endregion

            var contract = httpRequest.Files["contract"];
            #region Check Contract

            if (contract != null)
            {

                size = FileServices.CheckSize(contract);
                if (!size)
                {
                    return Ok("Cv Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(contract, "pdf doc docx");
                if (!type)
                {
                    return Ok("Contract Type must be pdf doc or docx!");
                }
            }
            #endregion

            var passport = httpRequest.Files["passport"];
            #region Check Passport

            if (passport != null)
            {
                size = FileServices.CheckSize(passport);
                if (!size)
                {
                    return Ok("Passport Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(passport, "pdf doc docx");
                if (!type)
                {
                    return Ok("Passport Type must be pdf doc or docx!");
                }
            }
            #endregion

            var diploma = httpRequest.Files["diploma"];
            #region Check Diploma
            if (diploma != null)
            {
                size = FileServices.CheckSize(diploma);
                if (!size)
                {
                    return Ok("diploma Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(diploma, "pdf doc docx");
                if (!type)
                {
                    return Ok("Diploma Type must be pdf doc or docx!");
                }
            }

            #endregion

            #region upload

            #region photo upload  
            if (photo != null)
            {
                string photoName = FileServices.UploadFile(photo, "~/Content/EmployeeDocuments/photo/",
                    true);
                model.Photo = photoName;
            }
            #endregion

            #region cv upload

            if (cv != null)
            {
                string cvName = FileServices.UploadFile(cv, "~/Content/EmployeeDocuments/cv/");
                model.Cv = cvName;
            }
            #endregion

            #region contract upload
            if (contract != null)
            {
                string contractName = FileServices.UploadFile(contract, "~/Content/EmployeeDocuments/contract/");
                model.Contract = contractName;
            }
            #endregion

            #region passport upload
            if (passport != null)
            {
                string passportName = FileServices.UploadFile(passport, "~/Content/EmployeeDocuments/passport/");
                model.Passport = passportName;
            }
            #endregion

            #region diploma upload
            if (diploma != null)
            {
                string diplomaName = FileServices.UploadFile(diploma, "~/Content/EmployeeDocuments/diploma/");
                model.Diploma = diplomaName;
            }
            #endregion

            #endregion

            var income = new Employee()
            {
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                GenderId = model.GenderId,
                MaritalStatus = (int)model.MaritalStatus,
                BirthDate = model.BirthDate,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                HireDate = model.HireDate,
                Photo = model.Photo,
                Cv = model.Cv,
                Contract = model.Contract,
                Passport = model.Passport,
                Diploma = model.Diploma,
                PassportSerial = model.PassportSerial,
                Address = model.Address,
                Salary = model.Salary
            };

            _context.Employees.Add(income);
            _context.SaveChanges();

            var id = income.EmployeeId;
            if (id <= 0)
            {
                return NotFound();
            }
            var employeeJob = new EmployeeJob()
            {
                EmployeeId = id,
                JobId = model.JobId,
                StartDate = DateTime.Now,

            };

            _context.EmployeeJobs.Add(employeeJob);
            _context.SaveChanges();


            return Ok("success");
        }


        [HttpPut]
        public async Task<IHttpActionResult> UpdateEmployee()
        {
            var size = true;
            var type = true;

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var filesReadToProvider = await Request.Content.ReadAsMultipartAsync();

            var json = await filesReadToProvider.Contents[0].ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<EmployeeUpdateBindingModel>(json);

            ModelState.Clear();
            this.Validate(model);
            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }

            var httpRequest = HttpContext.Current.Request;
            var photo = httpRequest.Files["photo"];
            #region Check photo
            if (photo != null)
            {
                size = FileServices.CheckSize(photo);
                if (!size)
                {
                    return Ok("Image Size exceeds maximum size. Foto must be smaller than 2MB!");
                }

                type = FileServices.CheckType(photo, "jpg png jpeg");
                if (!type)
                {
                    return Ok("Image Type must be jpg png or jpeg!");
                }
            }
            #endregion

            var cv = httpRequest.Files["cv"];
            #region Check CV
            if (cv != null)
            {
                size = FileServices.CheckSize(cv);
                if (!size)
                {
                    return Ok("Cv Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(cv, "pdf doc docx");
                if (!type)
                {
                    return Ok("CV Type must be pdf doc or docx!");
                }
            }

            #endregion

            var contract = httpRequest.Files["contract"];
            #region Check Contract

            if (contract != null)
            {
                size = FileServices.CheckSize(contract);
                if (!size)
                {
                    return Ok("Cv Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(contract, "pdf doc docx");
                if (!type)
                {
                    return Ok("Contract Type must be pdf doc or docx!");
                }
            }
            #endregion

            var passport = httpRequest.Files["passport"];
            #region Check Passport

            if (passport != null)
            {
                size = FileServices.CheckSize(passport);
                if (!size)
                {
                    return Ok("Passport Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(passport, "pdf doc docx");
                if (!type)
                {
                    return Ok("Passport Type must be pdf doc or docx!");
                }
            }
            #endregion

            var diploma = httpRequest.Files["diploma"];
            #region Check Diploma
            if (diploma != null)
            {
                size = FileServices.CheckSize(diploma);
                if (!size)
                {
                    return Ok("diploma Size exceeds maximum size. cv must be smaller than 2MB!");
                }

                type = FileServices.CheckType(diploma, "pdf doc docx");
                if (!type)
                {
                    return Ok("Diploma Type must be pdf doc or docx!");
                }
            }

            #endregion

         


            //! checks for model  
          
            #region upload

            #region photo upload  
            if (photo != null)
            {
                FileServices.DeleteFile(model.Photo, "~/Content/EmployeeDocuments/photo/");
                FileServices.DeleteFile(model.Photo, "~/Content/EmployeeDocuments/photo/photo200x200/");

                string photoName = FileServices.UploadFile(photo, "~/Content/EmployeeDocuments/photo/",
                    true);
                model.Photo = photoName;
            }
            #endregion

            #region cv upload

            if (cv != null)
            {
                FileServices.DeleteFile(model.Cv, "~/Content/EmployeeDocuments/cv/");
                string cvName = FileServices.UploadFile(cv, "~/Content/EmployeeDocuments/cv/");
                model.Cv = cvName;
            }
            #endregion

            #region contract upload
            if (contract != null)
            {
                FileServices.DeleteFile(model.Contract, "~/Content/EmployeeDocuments/contract/");
                string contractName = FileServices.UploadFile(contract, "~/Content/EmployeeDocuments/contract/");
                model.Contract = contractName;
            }
            #endregion

            #region passport upload
            if (passport != null)
            {
                FileServices.DeleteFile(model.Passport, "~/Content/EmployeeDocuments/passport/");
                string passportName = FileServices.UploadFile(passport, "~/Content/EmployeeDocuments/passport/");
                model.Passport = passportName;
            }
            #endregion

            #region diploma upload
            if (diploma != null)
            {
                FileServices.DeleteFile(model.Diploma, "~/Content/EmployeeDocuments/diploma/");
                string diplomaName = FileServices.UploadFile(diploma, "~/Content/EmployeeDocuments/diploma/");
                model.Diploma = diplomaName;
            }
            #endregion

            #endregion

            var income = _context.Employees.Find(model.EmployeeId);
            if (income == null)
            {
                return Ok("no data found with this id");
            }
            income = new Employee()
            {
                EmployeeId = model.EmployeeId,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                GenderId = model.GenderId,
                MaritalStatus = (int)model.MaritalStatus,
                BirthDate = model.BirthDate,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                HireDate = model.HireDate,
                Photo = model.Photo,
                Cv = model.Cv,
                Contract = model.Contract,
                Passport = model.Passport,
                Diploma = model.Diploma,
                PassportSerial = model.PassportSerial,
                Address = model.Address,
                Salary = model.Salary
            };

            //_context.Entry(income).State = EntityState.Modified;
            _context.Set<Employee>().AddOrUpdate(income);

            _context.SaveChanges();
            return Ok("Employee successfully Updated");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
