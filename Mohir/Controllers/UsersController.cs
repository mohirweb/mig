﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing.Text;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Mohir.App_Start;
using Mohir.Models;
using Mohir.ViewModels;

namespace Mohir.Controllers
{
    public class UsersController : ApiController
    {
        private ApplicationDbContext _context = new ApplicationDbContext();

        
        [HttpGet]
        public IHttpActionResult GetUsersWithRoles()
        {
            var usersWithRoles = (from user in _context.Users
                                  select new
                                  {
                                      UserId = user.Id,
                                      Username = user.UserName,
                                      Email = user.Email,
                                      RoleNames = (from userRole in user.Roles
                                                   join role in _context.Roles on userRole.RoleId
                                                       equals role.Id
                                                   select role.Name).ToList()
                                  }).ToList().Select(p => new UsersViewModel()

                                  {
                                      UserId = p.UserId,
                                      UserName = p.Username,
                                      Email = p.Email,
                                      Role = string.Join(",", p.RoleNames)
                                  });

            return Ok(usersWithRoles);

        }

        [HttpGet]
        public IHttpActionResult GetUsersWithRoles(int id)
        {
            var usersWithRoles = (from user in _context.Users
                                  select new
                                  {
                                      UserId = user.Id,
                                      Username = user.UserName,
                                      Email = user.Email,
                                      RoleNames = (from userRole in user.Roles
                                                   join role in _context.Roles on userRole.RoleId
                                                       equals role.Id
                                                   select role.Name).ToList()
                                  }).Where(l => l.UserId == id).ToList().Select(p => new UsersViewModel()

                                  {
                                      UserId = p.UserId,
                                      UserName = p.Username,
                                      Email = p.Email,
                                      Role = string.Join(",", p.RoleNames)
                                  });

            return Ok(usersWithRoles);
        }

        [HttpGet]
        public IHttpActionResult GetUserWithId()
        {

            var usersWithId = _context.Users.Select(s=>new
            {
                id = s.Id,
                userName = s.UserName,
                Email =s.Email,
                EmployeeId = s.EmployeeId
            }).ToList();
            return Ok(usersWithId);
        }

        [HttpGet]
        public IHttpActionResult GetRoles()
        {
            var roleManager = new ApplicationRoleManager(new MyRoleStore(_context));

            var roles = roleManager.Roles.ToList();
            return Ok(roles);
        }

        [HttpGet]
        public IHttpActionResult GetRoles(int id)
        {
            var roleManager = new ApplicationRoleManager(new MyRoleStore(_context));

            var role = roleManager.Roles.Where(s => s.Id == id).ToList();
            return Ok(role);
        }

        [HttpPost]
        public IHttpActionResult CreateRole(RoleBindingModel model)
        {
            var roleManager = new RoleManager<MyRole, int>(new MyRoleStore(_context));
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!roleManager.RoleExists(model.RoleName))
            {
                var role = new MyRole { Name = model.RoleName };
                roleManager.Create(role);

            }
            else
            {
                return Ok("role exists");
            }

            return Ok();

        }

        public IHttpActionResult UpdateRole(MyRole model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Entry(model).State = EntityState.Modified;
            _context.SaveChanges();
            return Ok();
        }


    }
}
