﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using Mohir.Models;
using EntityState = System.Data.Entity.EntityState;

namespace Mohir.Controllers
{
    public class AttendanceController : ApiController
    {
        HRContext _context = new HRContext();

        [HttpPost]
        public IHttpActionResult EmployeeEnter(int id)
        {
            var employee = _context.Employees.Find(id);
            if (employee != null)
            {
                var today = DateTime.Now.Date;
                var todayWasHere = _context.AttendanceLists.Any(s => s.Arrivaltime >= today && s.EmployeeId == id);
                if (!todayWasHere)
                {
                    var attendance = new AttendanceList()
                    {
                        Arrivaltime = DateTime.Now,
                        EmployeeId = id
                    };
                    _context.AttendanceLists.Add(attendance);
                    _context.SaveChanges();
                }
                else return Ok("record already exists");
            }
            return Ok("Record successfully created");
        }
        [HttpPost]
        public IHttpActionResult EmployeeDepart(int id)
        {
            //var record = _context.AttendanceLists.First(s => s.Arrivaltime.Date == DateTime.Now.Date && s.EmployeeId == id);
            var today = DateTime.Now.Date;
            var todayWasHere = _context.AttendanceLists.Any(s => s.Arrivaltime >= today && s.EmployeeId == id);
            if (todayWasHere)
            {
                var recordId = _context.AttendanceLists.Where(s => s.Arrivaltime >= today && s.EmployeeId == id).Select(u=>new
                {
                    id = u.Id,
                    Arrivaltime = u.Arrivaltime,
                    Departuretime=u.Departuretime
                }).Single();

                if (recordId.Departuretime != null) return Ok("Salomatit Xuba hast??");
                var attendance = new AttendanceList()
                {
                    //Arrivaltime = DateTime.Now,
                    //EmployeeId = id,
                    Id =recordId.id,
                    Arrivaltime = recordId.Arrivaltime,
                    Departuretime = DateTime.Now,
                    EmployeeId = id,
                };
                _context.Set<AttendanceList>().AddOrUpdate(attendance);
                _context.SaveChanges();


            }
            else return Ok("You have to click Arrival time firstly");

            return Ok("Bye Bye see you tomorrow");
        }


    }
}
