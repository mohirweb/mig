﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using ImageResizer;

namespace Mohir.Services
{
    public static class FileServices
    {
        public static bool CheckSize(HttpPostedFile file)
        {
            if (file.ContentLength > 2048 * 1024)
            {
                return false;
            }
            return true;
        }
        public static bool CheckType(HttpPostedFile file, string type)
        {
            var extension = Path.GetExtension(file.FileName).Trim('.');

            var fileType = type.ToLower().Split(' ').ToArray().Contains(extension.ToLower());
            if (!fileType)
            {
                return false;
            }

            return true;
        }
        public static string UploadFile(HttpPostedFile file, string path, bool resize = false)
        {

            var fileName = Guid.NewGuid().ToString();
            fileName = fileName.Replace(" ", "") + Path.GetExtension(file.FileName);
            var fileDestination = HttpContext.Current.Server.MapPath(path + fileName);
            if (resize)
            {
                ResizeSettings resizeSettings = new ResizeSettings()
                {
                    Width = 200,
                    Height = 200,
                    Mode = FitMode.Crop
                };
                ImageBuilder.Current.Build(file, "~/Content/EmployeeDocuments/photo/photo200x200/" + fileName, resizeSettings);
            }
            file.SaveAs(fileDestination);
            return fileName;
        }

        public static void DeleteFile(string fileName, string path)
        {
            var fullpath = HttpContext.Current.Server.MapPath(Path.Combine(path, fileName));
            if (File.Exists(fullpath))
            {
                File.Delete(fullpath);
            }

        }
    }
}